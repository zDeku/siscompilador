#!/usr/bin/env python
# Autor: Bruno Rodrigues Holanda
# Data: 14/09/2017
# Linguagem: python
from platform import mac_ver

from Class.util import AFD_lexico as Lex, Tabela as Tb


class Compilador(object):

    r = None
    i = 0
    j = 0
    tam = None
    alg = None
    saida = None
    linha = 1
    palavra = None
    estado_atual = None
    estado_anterior = None
    EOF = 0
    erro = 0
    virgola = 0


    '''
        Vetor de tokens: | token_1 | token_2 | token_3 | ...
                            ultimo             primeiro
    '''
    token_stack = []

    '''
        Retorna o proximo token a ser analisado.
        Se o tamanho do vetor de tokens for menor que 2 então insere no final um token até o vetor ter tamanho maior que 2.
        Se a pilha estiver vazia retorna None
    '''
    def next_token(self):
        if (len(self.token_stack) < 2) and (self.EOF == 0):

            while (len(self.token_stack) < 2) and (self.EOF == 0):
                token_str = self.get_token
                token = self.parse_token(token_str)
                self.token_stack.insert(0,token)

        token = {'nome': 'NaN', 'ident': 'NaN'}

        if not self.is_empty_token_stack():
            token = self.token_stack.pop()

        return token

    '''
        Insere o token no inicio do vetor
    '''
    def push_token(self, token):

        self.token_stack.append(token)

    '''
        Verifica se a pilha está vazia
    '''
    def is_empty_token_stack(self):
        return len(self.token_stack) == 0

    '''Implementação do Analisador lexico.
        Entrada:
            Algoritmo escrito em Lalgol.
        Saída:
            Token no formato: <valor,tipo>.
    '''
    @property
    def get_token(self):

        with open('entrada.alg', 'r') as file:

            if self.i == 0:
                self.alg = file.read()
                self.tam = self.alg.__len__()
                self.estado_atual = 'q0'
                self.estado_anterior = 'q0'
                self.saida = {}
                self.palavra = ''

            while self.i < self.tam:

                simbolo = self.alg[self.i]

                if simbolo is not '{' and not self.virgola:

                    self.estado_anterior = self.estado_atual
                    try:

                        self.estado_atual = Lex.AFD[self.estado_anterior][simbolo]
                    except KeyError:

                        self.saida[self.j] = '<' + simbolo + '-' + 'ERRO: Catactere inválido' + '> na linha: ' + self.linha.__str__()
                        self.j += 1
                        self.i += 1
                        self.palavra = ''
                        self.estado_anterior = 'q0'
                        self.estado_atual = 'q0'

                        return self.saida[self.j-1]

                    if self.verefica_estado(self.estado_atual):

                        result = self.get_resultado(self.palavra, self.estado_anterior)

                        if simbolo == ',':
                            self.i += 1
                            self.virgola = 1

                        self.saida[self.j] = '<' + self.palavra + '-' + result + '>'

                        self.j += 1
                        self.palavra = ''
                        self.estado_anterior = 'q0'
                        self.estado_atual = 'q0'

                        return self.saida[self.j-1]

                    if (simbolo is not ' ' and simbolo is not "\n"
                        and simbolo is not "\t" and simbolo is not ','):
                        self.palavra += simbolo

                    if simbolo == "\n":
                        self.linha += 1
                elif self.virgola:
                    self.virgola = 0
                    self.saida[self.j] = '<' + ',' + '-' + ',' + '>'
                    self.j += 1
                    return self.saida[self.j - 1]
                else:
                    self.linha += 1
                    while simbolo is not '}':
                        simbolo = self.alg[self.i]
                        self.i += 1

                self.i += 1


            result = self.get_resultado(self.palavra, self.estado_atual)
            self.saida[self.j] = '<' + self.palavra + '-' + result + '>'
            self.EOF = 1

            return self.saida[self.j]
    '''
        Implementação do analizador sintático recursivo usando rr(1)
    '''
    def parser(self):
        if not self.EOF:
            self.programa()

    def programa(self):
        if not self.EOF:
            mathCase = ['program', 'ident', ';']
            token = self.next_token()

            while (len(mathCase) > 0) and (not self.is_empty_token_stack()):

                key = mathCase.pop(0)

                if token['ident'] != key:
                    self.setErro(key, token)
                    self.push_token(token)

                token = self.next_token()

            self.push_token(token)

            self.corpo()

        if self.erro > 0:
            print('Não foi possível compilar o seu código!')
            print('Foram encontrados ', self.erro, 'erros no seu código')
        else:
            print('Programa compilado com sucesso!')

    def corpo(self):
        if not self.EOF:
            self.dc()

            token = self.next_token()

            if token['ident'] != 'begin':
                self.push_token(token)
                self.setErro('begin', token)

            self.comandos()

            token = self.next_token()

            if token['ident'] != 'end':
                self.push_token(token)
                self.setErro('end', token)

    def dc(self):
        if not self.EOF:
            token = self.next_token()

            if (token['ident'] == 'var'):
                self.push_token(token)
                self.dc_v()
                token = self.next_token()
            if (token['ident']) == 'procedure':
                self.push_token(token)
                self.dc_p()

    def dc_v(self):
        if not self.EOF:
            token = self.next_token()
            self.push_token(token)

            if token['ident'] == 'var':
                mathCase = ['var', ':', ';']
                key = mathCase.pop(0)
                token = self.next_token()


                if token['ident'] != key:
                    self.setErro(key, token)
                    self.push_token(token)

                self.variaveis()

                token = self.next_token()
                key = mathCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro(key, token)

                self.tipo_var()

                token = self.next_token()
                key = mathCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro(key, token)

                token = self.next_token()

                if (token['ident'] == 'var'):
                    self.push_token(token)
                    self.dc_v()
                else:
                    self.push_token(token)

    def tipo_var(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] != 'integer' and token['ident'] != 'real':
                self.setErro('integer ou real', token)
                self.push_token(token)

    def variaveis(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] != 'ident':
                self.push_token(token)
                self.setErro('ident', token)

            self.mais_var()

    def mais_var(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] == ',':
                self.variaveis()
            else:
                self.push_token(token)

    def dc_p(self):
        if not self.EOF:
            mathCase = ['procedure', 'ident', ';']
            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            self.parametros()

            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            self.corpo_p()

    def corpo_p(self):
        if not self.EOF:
            mathCase = ['begin', 'end', ';']

            self.dc_loc()

            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            self.comandos()

            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            token = self.next_token()
            key = mathCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

    def comandos(self):
        if not self.EOF:
            token = self.next_token()

            if (token['ident'] == 'read' or token['ident'] == 'write' or
                token['ident'] == 'while' or token['ident'] == 'if' or
                token['ident'] == 'ident' or token['ident'] == 'begin'):

                self.push_token(token)
                self.cmd()

                token = self.next_token()

                if token['ident'] != ';':
                    self.push_token(token)
                    self.setErro(';', token)

                self.comandos()
            else:
                self.push_token(token)

    def cmd(self):
        if not self.EOF:
            machCase = None;
            token = self.next_token()
            proxToken = self.next_token()

            if token['ident'] == 'read':
                machCase = ['(', ')']
                token = proxToken
                key = machCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro('comando', token)

                self.variaveis()

                token = self.next_token()
                key = machCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro('comando', token)

            elif token['ident'] == 'write':
                machCase = ['(', ')']
                token = proxToken
                key = machCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro('comando', token)

                self.variaveis()

                token = self.next_token()
                key = machCase.pop(0)

                if token['ident'] != key:
                    self.push_token(token)
                    self.setErro('comando', token)
            elif token['ident'] == 'while':
                self.push_token(proxToken)
                self.condicao()

                token = self.next_token()

                if token['ident'] != 'do':
                    self.push_token(token)
                    self.setErro('do', token)
                self.cmd()

            elif token['ident'] == 'if':
                self.push_token(proxToken)
                self.condicao()

                token = self.next_token()

                if token['ident'] != 'then':
                    self.push_token(token)
                    self.setErro('then', token)

                self.cmd()

                self.pfalsa()

            elif token['ident'] == 'ident' and proxToken['ident'] == ':=':
                token = proxToken

                if token['ident'] != ':=':
                    self.push_token(token)
                    self.setErro(':=', token)

                self.expressao()

            elif token['ident'] == 'ident':
                self.push_token(proxToken)
                self.lista_arg()

            elif token['ident'] == 'begin':
                self.push_token(proxToken)
                self.comandos()

                token = self.next_token()

                if token['ident'] != 'end':
                    self.push_token(token)
                    self.setErro('end', token)
            else:
                self.push_token(token)
                self.setErro('comando', token)

    def lista_arg(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] != '(':
                self.push_token(token)
                self.setErro('(', token)

            self.argumentos()

            token = self.next_token()

            if token['ident'] != ')':
                self.push_token(token)
                self.setErro(')', token)

    def argumentos(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] != 'ident':
                self.push_token(token)
                self.setErro('ident', token)
            self.mais_ident()

    def mais_ident(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] == ';':
                self.argumentos()
            else:
                self.push_token(token)

    def pfalsa(self):
        if not self.EOF:
            token = self.next_token()

            if token['ident'] == 'else':
                self.cmd()
            else:
                self.push_token(token)

    def condicao(self):
        if not self.EOF:
            self.expressao()
            self.relacao()
            self.expressao()

    def relacao(self):
        if not self.EOF:
            token = self.next_token()

            if (token['ident'] == '=' or token['ident'] == '<>' or
                token['ident'] == '>=' or token['ident'] == '<=' or
                token['ident'] == '>' or token['ident'] == '<'):
                pass
            else:
                print(self.saida)
                self.push_token(token)
                self.setErro('=, <>, >=, <=, >, <', token)

    def expressao(self):
        if not self.EOF:
            self.termo()
            self.outros_termos()

    def outros_termos(self):
        if not self.EOF:
            token = self.next_token()
            self.push_token(token)

            if (token['ident'] == '+') or (token['ident'] == '-'):
                self.op_ad()
                self.termo()
                self.outros_termos()

    def op_ad(self):
        if not self.EOF:
            token = self.next_token()

            if (token['ident'] != '+') and (token['ident'] != '-'):
                self.push_token(token)
                self.setErro('+ ou -', token)

    def termo(self):
        if not self.EOF:
            self.op_un()
            self.fator()
            self.mais_fatores()

    def mais_fatores(self):
        if not self.EOF:
            token = self.next_token()
            self.push_token(token)

            if (token['ident'] == '*') or (token['ident'] == '/'):
                self.op_mul()
                self.fator()
                self.mais_fatores()

    def op_un(self):
        if not self.EOF:
            token = self.next_token()

            if (token['ident'] != '+') or (token['ident'] != '-'):
                self.push_token(token)

    def fator(self):
        if not self.EOF:
            token = self.next_token()
            if token['ident'] == 'ident' or 'num':
                pass
            elif token['ident'] == '(':

                self.expressao()

                token = self.next_token()

                if token['ident'] != ')':
                    self.push_token(token)
                    self.setErro('ident ou num_real ou num_int', token)
            else:
                self.setErro('num, ident ou (', token)
                self.push_token(token)



    def dc_loc(self):
        if not self.EOF:
            self.dc_v()


    def parametros(self):
        if not self.EOF:
            machCase = ['(', ')']
            token = self.next_token()
            key = machCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            self.lista_par()

            token = self.next_token()
            key = machCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

    def lista_par(self):
        if not self.EOF:
            machCase = [':', ';']

            self.variaveis()

            token = self.next_token()
            key = machCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            self.tipo_var()

            token = self.next_token()
            key = machCase.pop(0)

            if token['ident'] != key:
                self.push_token(token)
                self.setErro(key, token)

            token = self.next_token()

            if token['ident'] == 'ident':
                self.push_token(token)
                self.lista_par()
            else:
                self.push_token(token)

    def parse_token(self, token):

        if (token.count('<') == 1) and (token.count('>') == 1):
            token = token.replace('>', '').replace('<', '')
            arr = token.split('-')
            obj = {'nome': arr[0], 'ident': arr[1]}
        elif token.count('<') > 1:
            obj = {'nome': '<', 'ident': '<'}
        elif token.count('>') > 1:
            obj = {'nome': '>', 'ident': '>'}

        return obj

    def verefica_estado(self, estado):

        try:
            self.r = Tb.erro[estado]
        except KeyError:
            self.r = None

        return self.r is not None

    @staticmethod
    def get_resultado(palavra, estado_anterior):
        if estado_anterior == '*q2':
            try:
                result = Tb.simbolo[palavra]
            except KeyError:
                result = 'ident'
        else:
            try:
                result = Tb.simbolo[estado_anterior]
            except KeyError:
                result = Tb.erro[estado_anterior]
            except KeyError:
                result = 'ERRO: Desconhecido'

        return result

    def setErro(self, key, token):
        print('Erro token inesperado na linha', self.linha, 'encontrado', token['nome'], 'esperando', key)
        self.erro += 1